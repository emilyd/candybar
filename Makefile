install:
	cp candybar.sh /usr/local/bin/candybar

config-install:
	mkdir ~/.config/candybar/
	cp modules.sh ~/.config/candybar/
	cp config.sh ~/.config/candybar/
