# candybar
a stupidly simple modular statusbar

![preview 0](https://img.donut.gq/1661519970.png)
![preview 1](https://cdn.discordapp.com/attachments/961749313480953887/1012981184764059768/image.png)
![preview 2](https://imgs.asia/images/2022/08/27/2022-08-27-092211_3840x1080_scrot.png)

## usage
**candybar** only outputs the text that should be shown on the bar, which makes it useable on different applications.
### usage with dwm
add the following to your ~/.xinitrc:
```sh
while true; do
	xsetroot -name "$(candybar)"
	sleep 2
done &
```
### usage with tmux
add the following to your ~/.tmux.conf:
```tmux
set -g status-interval 2
set -g status-right-length 100
set -g status-right "#(candybar)"
```
### usage with zsh
add the following to your ~/.zshrc:
```zsh
precmd() {
	psvar[1]="$(candybar)"
}
export CSI=$'\e'"["
export PROMPT="${CSI}s${CSI}1;$((LINES-1))r${CSI}$LINES;1f%S%1v%s${CSI}K${CSI}u${PROMPT}"
```

## writing modules
writing candybar modules is very easy. modules can be written in any programming language.

first, open up the modules.sh file. create a new function with, for example, the name `module_foo()`.
```sh
module_foo() {
	# uh, so what now?
}
```
next, if you are using POSIX SH to write your module, just write your script in the function.

for this example, we'll be writing a module that outputs the current date.
```sh
module_foo() {
	# remember to keep the ${bar}, ${1}, and ${2} here!
	export bar="${bar}${1}$(date)${2}"
}
```
if you are using a different language, write your module and then run it with sh.
```sh
module_foo() {
	export output="$(/path/to/program)"
	export bar="${bar}${1}${output}${2}"
}
```
now add a line for your module in the config.sh, for example:
```sh
module_foo	"Date:"		" | "
```
you're done! run candybar to see the results.

remember that if you have configured candybar to be used in zsh, tmux or dwm like in this README, you won't have to restart candybar!
